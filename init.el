;; NOTE: THIS FILE CONTAINS PARTS OF THE REQUIRED EMACS INIT.EL FILE
;; FOR BUILDING THE PDF. IT IS NOT A STANDALONE THING.

;; ==========================
;; ORG-MODE SPECIFIC SETTINGS
;; ==========================
(require 'org)
(require 'ox-latex)
;; Setup for exporting to PDF using Xelatex First you need to define a
;; document class, which will be referred to later, that has all the
;; definitions required to compile the PDF.

;; EXAMPLE
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("org-book"
                 "
\\documentclass[12pt, a4paper, openany, twoside]{book}
\\usepackage[vmargin=2cm,
             rmargin=2cm,
             lmargin=2cm,
             bindingoffset=1cm]{geometry}
\\usepackage[UTF8]{ctex}
\\usepackage[autostyle=true]{csquotes}  % modify quotes
\\usepackage{amsmath}
\\usepackage{amssymb}
\\usepackage{mathtools}
\\usepackage{hyperref}

 %%%%%%%%%%%%%%%%
%%%%% IMAGES %%%%%
 %%%%%%%%%%%%%%%%
\\usepackage{caption}

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% BIBLIOGRAPHY AND CITING %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\\usepackage[square,sort,comma,numbers]{natbib}
\\bibliographystyle{alphadin}  %% does not work with biblatex
\\renewcommand*{\\bibfont}{\\raggedright}

 %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% TABLE OF CONTENTS %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%
% styling for article document class
% \\usepackage{tocloft}
\\usepackage[rightlabels, dotinlabels]{titletoc}
\\setcounter{chapter}{1}

% level of depth of table of contents and section numberings
\\setcounter{tocdepth}{2}
\\setcounter{secnumdepth}{2}

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% LANGUAGE SETTINGS %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% NOTE: happens in each document, not in this template
% (important for automatic breaking words)
% \\selectlanguage{ngerman}
% \\selectlanguage{USenglish}

 %%%%%%%%%%%%%%%%%%%%
%%%%% PAGE STYLE %%%%%
 %%%%%%%%%%%%%%%%%%%%
\\raggedbottom
\\pagestyle{empty}


\\title{}
     [NO-DEFAULT-PACKAGES]
      [NO-PACKAGES]

 %%%%%%%%%%%%%%%%%%
%%%%% HEADINGS %%%%%
 %%%%%%%%%%%%%%%%%%
\\usepackage{titlesec}
\\titleformat{\\chapter}[hang]{\\fontsize{22pt}{23pt}\\bfseries}{\\thechapter.\\quad}{0pt}{}{}
\\titleformat{\\section}[hang]{\\fontsize{16pt}{17pt}\\bfseries}{\\thesection.\\quad}{0pt}{}
\\titleformat{\\subsection}[hang]{\\fontsize{14pt}{15pt}\\bfseries}{\\thesubsection.\\quad}{0pt}{}
\\titleformat{\\subsubsection}[hang]{\\fontsize{13pt}{14pt}\\bfseries}{\\thesubsubsection\\quad}{0pt}{}
\\titleformat{\\paragraph}[hang]{\\normalsize\\bfseries}{\\theparagraph\\quad}{0pt}{}

 %%%%%%%%%%%%%%%%%%%
%%%%% MATH MODE %%%%%
 %%%%%%%%%%%%%%%%%%%
\\newcommand{\\Hsquare}{%
    \\text{\\fboxsep=-.15pt\\fbox{\\rule{0pt}{.75ex}\\rule{.75ex}{0pt}}}%
}

\\newcommand{\\divides}{\\mid}
\\newcommand{\\notdivides}{\\nmid}
\\newcommand{\\setR}{\\mathbb{R}}
\\newcommand{\\setQ}{\\mathbb{Q}}
\\newcommand{\\setZ}{\\mathbb{Z}}
\\newcommand{\\setN}{\\mathbb{N}}

\\usepackage{etoolbox}% for '\\AtBeginEnvironment' macro
\\AtBeginEnvironment{pmatrix}{\\everymath{\\displaystyle}}
\\AtBeginEnvironment{bmatrix}{\\everymath{\\displaystyle}}
\\AtBeginEnvironment{matrix}{\\everymath{\\displaystyle}}
\\AtBeginEnvironment{array}{\\everymath{\\displaystyle}}

% absolute value
\\DeclarePairedDelimiter\\abs{\\lvert}{\\rvert}
\\DeclarePairedDelimiter\\norm{\\lVert}{\\rVert}

% Swap the definition of \\abs* and \\norm*, so that \\abs
% and \\norm resizes the size of the brackets, and the
% starred version does not.
\\makeatletter
\\let\\oldabs\\abs
\\def\\abs{\\@ifstar{\\oldabs}{\\oldabs*}}
%
\\let\\oldnorm\\norm
\\def\\norm{\\@ifstar{\\oldnorm}{\\oldnorm*}}
\\makeatother

\\newcommand*\\colvec[1]{
  \\begin{pmatrix}
    #1
  \\end{pmatrix}
}

 %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PREDEFINED COLORS %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%
\\usepackage{color}
\\usepackage[table]{xcolor}
\\definecolor{quotecolor}{HTML}{686868}
\\definecolor{White}{RGB}{255, 255, 255}
\\definecolor{Black}{RGB}{0, 0, 0}
\\definecolor{tableHeader}{RGB}{211, 47, 47}
\\definecolor{tableLineOdd}{RGB}{245, 245, 245}
\\definecolor{tableLineEven}{RGB}{224, 224, 224}
\\definecolor{linkgray}{RGB}{100, 100, 100}
\\definecolor{CadetBlue}{RGB}{110, 106, 156}

 %%%%%%%%%%%%%%%%%%%
%%%%% FOOTNOTES %%%%%
 %%%%%%%%%%%%%%%%%%%
\\makeatletter
\\def\\@footnotecolor{red}
\\define@key{Hyp}{footnotecolor}{%
    \\HyColor@HyperrefColor{#1}\\@footnotecolor%
}
\\patchcmd{\\@footnotemark}{\\hyper@linkstart{link}}{\\hyper@linkstart{footnote}}{}{}
\\makeatother

 %%%%%%%%%%%%%%%
%%%%% FONTS %%%%%
 %%%%%%%%%%%%%%%
\\setCJKmainfont[Scale=1.0]{WenQuanYi Micro Hei}
\\setmainfont[Scale=1]{Ubuntu}
\\setmonofont{Liberation Mono}
\\DeclareMathSizes{14}{12}{10}{8}

 %%%%%%%%%%%%%%%%%%%%
%%%%% HYPERLINKS %%%%%
 %%%%%%%%%%%%%%%%%%%%
%% DEPENDENCIES: FOOTNOTES block
\\hypersetup{
    colorlinks=true,
    urlcolor=CadetBlue,
    filecolor=linkgray,
    citecolor=blue,
    linkcolor=linkgray,
    footnotecolor=blue
}
\\urlstyle{same}

 %%%%%%%%%%%%%%%%%%%
%%%%% ALIGNMENT %%%%%
 %%%%%%%%%%%%%%%%%%%
% default is justified alignment

 %%%%%%%%%%%%%%%%%%%%%
%%%%% HYPHENATION %%%%%
 %%%%%%%%%%%%%%%%%%%%%
\\pretolerance=5000
\\tolerance=9000
\\emergencystretch=0pt
\\righthyphenmin=2
\\lefthyphenmin=4
"
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(setq org-latex-pdf-process
      '("xelatex -interaction nonstopmode %f"
        "xelatex -interaction nonstopmode %f")) ;; for multiple passes
